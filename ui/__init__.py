from core import game, player
from core.game import *
from core.player import *
print("  _________                      __    ________                   __                        __  .__               ")
print(" /   _____/_  _  __ ____   _____/  |_  \______ \   ____   _______/  |________ __ __   _____/  |_|__| ____   ____  ")
print(" \_____  \  \/ \/ // __ \_/ __ \   __\  |    |  \_/ __ \ /  ___/\   __\_  __ \  |  \_/ ___\   __\  |/  _ \ /    \ ")
print(" /        \      /\  ___/\  ___/|  |    |    `   \  ___/ \___ \  |  |  |  | \/  |  /\  \___|  | |  (  <_> )   |  \ ")
print("/_______  / \/\_/  \___  >\___  >__|   /_______  /\___  >____  > |__|  |__|  |____/  \___  >__| |__|\____/|___|  /")
print("        \/             \/     \/               \/     \/     \/                          \/                    \/  \n \n")

opt1 = int(input("(1) NEW GAME\n(2) EXIT GAME\nTYPE YOUR OPTION: "))

#Easy: 5x5 | Attempts 25  |||  Medium: 2: 7x7 | Attempts 20   ||| Hard : 10x10 | Attempts 15  ||| Extreme : 15x15 | 10 Attempts
helper = []
atem = None


def menu():

    if opt1 == 1:
        name = str(input("WRITE YOUR NAME: "))
        #new_player(name)
        print("(1) EASY:    5x5   || 25 Attempts")
        print("(2) MEDIUM:  7x7   || 20 Attempts")
        print("(3) HARD:    10x10 || 15 Attempts")
        print("(4) EXTREME: 15x15 || 10 Attempts")
        opt2 = int(input("TYPE YOUR OPTION: "))
        if opt2 == 1:
            c = "Easy"
        elif opt2 == 2:
            c = "Medium"
        elif opt2 == 3:
            c = "Hard"
        elif opt2 == 4:
            c = "Extreme"
        else:
            print("ERROR: Unrecognized Character")
        global helper
        helper.append(name)
        helper.append(c)
    else:
        exit()


def game():

    current_game = new_game(helper[1])

    global atem
    current_game.Board.new_board()
    current_game.Board.show_board()
    while current_game.getAttempts() > 0:
        print("---------------------------------------------------------------------------------------------")
        print(current_game.Board.explosions, " Explosions were Made")
        print(current_game.Board.crushed_candys, " Candys were Crushed")
        print(current_game.player.show_attempts(), "Attempts Left")
        print("---------------------------------------------------------------------------------------------")
        print("Type the coordinates of the Candy you wish to Change.")
        row = int(input("Row: "))

        col = int(input("Column: "))

        change = int(input("Which side will you change?\n(1) Right\n(2) Left\n(3) Up\n(4) Down\nType: "))
        if change == 1:
            current_game.Board.change_piece(row, col, "RIGHT")
        elif change ==2:
            current_game.Board.change_piece(row, col, "LEFT")
        elif change == 3:
            current_game.Board.change_piece(row, col, "UP")
        elif change == 4:
            current_game.Board.change_piece(row, col, "DOWN")

        current_game.Board.explosion()

        current_game.Board.show_board()
        current_game.player.update_attempts(current_game.player.show_attempts() - 1)

        current_game.Board.fill_space()
        print("\n \n")
        current_game.Board.show_board()






def main():


    menu()
    game()
main()


