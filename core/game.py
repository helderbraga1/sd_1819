from core import LIST_LEVEL
from core.board import Board
from core.player import Player


class Game:

    def __init__(self, level):
        self.level = level
        self.player = Player(None)

        if self.level == LIST_LEVEL[0][0]:
            self.Board = Board(LIST_LEVEL[0][1], LIST_LEVEL[0][2])
            self.player.update_attempts(LIST_LEVEL[0][3])
        elif self.level == LIST_LEVEL[1][0]:
            self.Board = Board(LIST_LEVEL[1][1], LIST_LEVEL[1][2])
            self.player.update_attempts(LIST_LEVEL[1][3])
        elif self.level == LIST_LEVEL[2][0]:
            self.Board = Board(LIST_LEVEL[2][1], LIST_LEVEL[2][2])
            self.player.update_attempts(LIST_LEVEL[2][3])
        elif self.level == LIST_LEVEL[3][0]:
            self.Board = Board(LIST_LEVEL[3][1], LIST_LEVEL[3][2])
            self.player.update_attempts(LIST_LEVEL[3][3])

    def getAttempts(self):
        return self.player.show_attempts()

def new_game(level):
    return Game(level)

def new_player(name):
    k = Player(name)



