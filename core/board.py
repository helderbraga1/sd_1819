from core.candy import GenerateCandy

class Board:

    def __init__(self, rows, cols):
        self.rows = rows
        self.cols = cols
        self.game_board = [[None for i in range(rows)] for j in range(cols)]
        self.crushed_candys = 0
        self.explosions = 0

    def new_board(self):
        for i in range(self.cols):
            for j in range(self.rows):
                self.game_board[i][j] = GenerateCandy()

    def show_board(self):
        for c in range(self.cols):
            for r in range(self.rows):
                print(self.game_board[c][r].board_symbol, end=" ")
            print(' ')

    def change_piece(self,row,column,direction):
        if(direction == "UP"):
            if row == 0:
                print("You cant go up because you're on the first row")
            else:
                old = self.game_board[row-1][column]
                self.game_board[row-1][column] = self.game_board[row][column]
                self.game_board[row][column] = old
        if (direction == "DOWN"):
            if row == self.rows - 1:
                print("You cant go down because you're on the last row")
            else:
                old = self.game_board[row + 1][column]
                self.game_board[row + 1][column] = self.game_board[row][column]
                self.game_board[row][column] = old
        if (direction == "RIGHT"):

            if column == self.cols - 1:

                print("You cant go right because you're on the last column")
            else:
                old = self.game_board[row][column + 1]
                self.game_board[row][column + 1] = self.game_board[row][column]
                self.game_board[row][column] = old
        if (direction == "LEFT"):
            if column == 0:
                print("You cant go left because you're on the first column")
            else:
                old = self.game_board[row][column - 1]
                self.game_board[row][column - 1] = self.game_board[row][column]
                self.game_board[row][column] = old
    def explosion(self):
        lista_explosao = [] #lista que guardará as casas que irao explodir

        equal_candy = 1 # se encontrar casas iguais ela vai aumentando neste contador em +1 (se esta variavel chegar a 3 entao é uma explosao)

       #----------------------------------------EXPLOSOES NA HORIZONTAL ----------------------------------------------------
        for j in range(self.rows):  # ciclo simples
            aux = 0  # variavel auxiliar qe vai ser utilizada pra comparar os elementos duma casa com a casa em frente e ver se são iguais
            for i in range(self.cols): #ciclo simples

                if aux == self.game_board[j][i].board_symbol and aux != " ": #Compara o valor da  var "aux" com a casa em frente utilizando o contador
                    equal_candy = equal_candy + 1  #Adiciona ao contador se o candy for igual

                else:
                    if equal_candy >= 3: # Ele aqui verifica se o valor do equal_candy era maior do qe 3 nas contagens anteriores

                        if self.game_board[j][i-1].board_symbol == "=": # Este caso apaga a linha inteira porque é o candy "=" da horizontal
                            a = 0
                            while a < self.cols:
                                lista_explosao.append([j, a])
                                a = a + 1



                        elif self.game_board[j][i-1].board_symbol == "H": # Este apaga as colunas inteiras porque é o candy H da vertical
                            x = 1
                            while x < equal_candy + 1:
                                b = 0
                                while b < self.rows:
                                    lista_explosao.append([b, i - x])
                                    b = b + 1

                                x = x + 1



                        else: # para os candys normais com 3 ou maior que 3


                            x = 1 # este contador começa a 1 para simbolizar a casa anterior no ciclo á frente para que depois se faça algo do genero  J-1 (O 1 é o X), J-2, J-3, J-4 por ai em diante até se adicionar todas as casas da explosao
                            while x < equal_candy + 1: # Começa em 1 porque o contador começa em 1 tmbm, se for maior do que 3 ele vai guardar estas casas na lista_explosao utilizando este ciclo que corre o numero de vezes do equal_candy
                                lista_explosao.append([j, i-x]) #Vai adicionando as coordenadas diminuindo da casa anterior para as anteriores das iteraçoes anteriores
                                x=x +1
                            self.explosions = self.explosions + 1 # Registar o numero de explosoes


                            if equal_candy > 3: #Se for maior que 3 ele vai adicionar um "=" qe simboliza um riscado

                                    self.game_board[j][i-1].board_symbol = "="
                                    lista_explosao.remove([j, i-1])


                    equal_candy = 1 # caso nao seja igual a variavel equal_candy volta ao valor 1
                    aux = self.game_board[j][i].board_symbol # A variavel auxiliar toma este novo valor para comparar com casas em frente

                # --->>>>>Este caso existe para o ultimo candy de cada linha--------------------------

                if i == self.cols -1:
                    if equal_candy >= 3:

                        if self.game_board[j][i].board_symbol == "=":  # Este caso apaga a linha inteira porque é o candy "=" da horizontal
                            a = 0
                            while a < self.cols:
                                lista_explosao.append([j, a])
                                a = a + 1



                        elif self.game_board[j][i].board_symbol == "H":
                            x = 0
                            while x < equal_candy + 1:
                                b = 0
                                while b < self.rows:
                                    lista_explosao.append([b, i - x])
                                    b = b + 1

                                x = x + 1
                        else:

                                x = 0  #Neste caso o contador começa a 0 porque estamos na ultima casa desta linha e queremos que ela seja adicionada
                                while x < equal_candy:
                                    lista_explosao.append([j,i - x])
                                    x = x + 1
                                self.explosions = self.explosions + 1 # registar o numero de explosoes


                                if equal_candy > 3:  # Se for maior que 3 ele vai adicionar um "=" qe simboliza um riscado
                                    self.game_board[j][i].board_symbol = "="
                                    lista_explosao.remove([j, i])
                        equal_candy = 1



     #--------------------------------------EXPLOSOES NA VERTICAL-------------------------------------------------------------------


        for j in range(self.cols):  # Mesma coisa de cima só que na vertical
            aux = 0  # reset do auxiliar
            for i in range(self.rows):
                if aux == self.game_board[i][j].board_symbol and aux != " ":
                    equal_candy = equal_candy + 1

                else:
                    if equal_candy >= 3:
                        if self.game_board[i - 1][j].board_symbol == "H":
                            a = 0
                            while a < self.rows:
                                lista_explosao.append([a, j])
                                a = a + 1

                        elif self.game_board[i - 1][j].board_symbol == "=":
                                x = 1
                                while x < equal_candy + 1:
                                    b = 0
                                    while b < self.cols:
                                        lista_explosao.append([i - x, b])
                                        b = b + 1

                                    x = x + 1
                        else:

                                x = 1
                                while x < equal_candy + 1:
                                    lista_explosao.append([i-x,j])
                                    x = x + 1
                                self.explosions = self.explosions + 1  # registar o numero de explosoes

                                if equal_candy > 3:  # Se for maior que 3 ele vai adicionar um "=" qe simboliza um riscado

                                    self.game_board[i - 1][j].board_symbol = "H"
                                    lista_explosao.remove([i -1, j])

                    equal_candy = 1
                    aux = self.game_board[i][j].board_symbol

                # --->>>>>Este caso existe para o ultimo candy de cada linha--------------------------

                if i == self.rows - 1:
                    if equal_candy >= 3:

                        if self.game_board[i][j].board_symbol == "H":
                            a = 0
                            while a < self.rows:
                                lista_explosao.append([a, j])
                                a = a + 1

                        elif self.game_board[i][j].board_symbol == "=":
                            x = 0
                            while x < equal_candy + 1:
                                b = 0
                                while b < self.cols:
                                    print(i-x, b)
                                    print(lista_explosao)
                                    lista_explosao.append([i - x, b])

                                    b = b + 1

                                x = x + 1
                        else:

                                x = 0  # Neste caso o contador começa a 0 porque estamos na ultima casa desta coluna e queremos que ela seja adicionada
                                while x < equal_candy:
                                    lista_explosao.append([i-x, j])
                                    x = x + 1
                                self.explosions = self.explosions + 1  # registar o numero de explosoes

                                if equal_candy > 3:  # Se for maior que 3 ele vai adicionar um "=" qe simboliza um riscado

                                    self.game_board[i][j].board_symbol = "H"
                                    lista_explosao.remove([i, j])

                        equal_candy = 1


        #REMOVE OS CANDYS DAS EXPLOSOES
        for i in lista_explosao:
            self.game_board[i[0]][i[1]].board_symbol = " "
            self.crushed_candys = self.crushed_candys + 1 # Conta o numero de explosoes

    def firstline(self): # Ciclo para gerar os candys na primeira linha
        for i in range(self.cols):

            if self.game_board[0][i].board_symbol == " ":
                self.game_board[0][i] = GenerateCandy()



    def fill_space(self):
        print("|||||||||||||||||||FILL SPACE|||||||||||||||||||||")
        for a in range(self.rows * self.cols):
            for i in range(self.rows):
                    for j in range(self.cols):
                        if self.game_board[i][j].board_symbol == " ":
                                self.firstline()
                                aux = self.game_board[i - 1][j].board_symbol
                                self.game_board[i - 1][j].board_symbol = self.game_board[i][j].board_symbol
                                self.game_board[i][j].board_symbol = aux
            spaces = 0
            for c in range(self.rows):
                for d in range(self.cols):
                    if self.game_board[c][d].board_symbol == " ":
                        spaces = spaces + 1
            if spaces == 0:
                a = self.rows * self.cols + 1
            else:
                print("\n \n")
                self.show_board()
                print("\n \n")


















#Como usar o tabuleiro abaixo:
'''
c = Board(3,3)                 ||| cria o tabuleiro com esse tamanho
c.new_board()                  ||| preenche as casas vazias
c.show_board()                 ||| mostra o tabuleiro
c.change_piece(1, 1, "LEFT")   ||| troca os candyz

'''