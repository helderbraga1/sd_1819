class Player:

    def __init__(self, username):
        self.username = username
        self.attempts = -100

    def update_attempts(self, attempts):
        self.attempts = attempts

    def show_attempts(self):
        return self.attempts
