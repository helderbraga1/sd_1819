from core import LIST_COLOR
from random import randint


class Candy:

    def __init__(self, color, board_symbol):
        self.color = color
        self.board_symbol = board_symbol


class Blue(Candy):

    def __init__(self):
        super().__init__(LIST_COLOR[0], "B")


class Red(Candy):

    def __init__(self):
        super().__init__(LIST_COLOR[1], "R")


class Green(Candy):

    def __init__(self):
        super().__init__(LIST_COLOR[2], "G")


class CandyH(Candy):

    def __init__(self):
        super().__init__(LIST_COLOR[3], "=")


class CandyV(Candy):

    def __init__(self):
        super().__init__(LIST_COLOR[4], "H")


def GenerateCandy():
    random = randint(0, 100)
    if(random <= 25):
        c = Blue()
    elif (25 < random < 45):
        c = Red()
    elif (45 <= random < 60):
        c = Green()
    elif (60 <= random < 75):
        c = CandyH()
    elif (75 <= random <= 100):
        c = CandyV()
    return c